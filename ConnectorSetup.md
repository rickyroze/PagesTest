---
layout: page
title: Connector Setup
permalink: /connectorsetup/
---
# Connector Setup Instructions.

## Intro:

The purpose of this document is to allow you to easily switch between our tack sensor and the Homerus tack sensor using a standard waterproof connector.

## What is included:
* Tack sensor
* Male connector
* Female connector (_X2_)

## What you will need:
* Homerus tack sensor system
* Wire cutters/strippers
* 2mm flathead screwdriver
* Soldering iron _(optional but preferred)_

### Getting Started:

1. Cut the wire off of the new tack sensor, leaving around 10-15 cm still attached.
2. Strip off about 3 cm of the outer shielding leaving the 3 inner wires exposed. _(The instructions from here on are to be used on the new and old tack sensors)_
3. Strip off about 1 cm of the shielding from the ends of each wire.
4. Tin each wire with solder for ease of installation and a more solid connection _(optional)_. _The end result should look like the picture below:_

<a href="https://drive.google.com/uc?id=1Z3OUldAsjI05VXoYu_Hq9xgZHW5KPdX4" target="_blank"><img src="https://drive.google.com/uc?id=1Z3OUldAsjI05VXoYu_Hq9xgZHW5KPdX4" 
alt="Step 1" width="360" height="270" border="10" /></a>

### Wiring the Connectors:
5. The _female connectors_ go on each __tack sensor__ while the _male connector_ go from the __siren box__.
6. Place parts of the connector over the wire in the order shown below _(from left to right)_:
    1. Black screw piece
    2. White plastic piece
    3. Black rubber seal
    4. Black plastic collar
7. Arrange wires from top to bottom: __Green__, __Brown__, __White__.
8. Position the connector such that the center screw terminal is furthest from the end of the wire.
9. Back the set screws off almost all the way _(don't take them all the way out, they are not fun to lose)_.

<a href="https://drive.google.com/uc?id=11hnMpyINCIi0deB9e45J2_QI6u2Ftel6" target="_blank"><img src="https://drive.google.com/uc?id=11hnMpyINCIi0deB9e45J2_QI6u2Ftel6" 
alt="Step 7" width="360" height="270" border="10" /></a>
<a href="https://drive.google.com/uc?id=1JPLGSSEcDkP6DoaUjF0fQliXhIcbCct_" target="_blank"><img src="https://drive.google.com/uc?id=1JPLGSSEcDkP6DoaUjF0fQliXhIcbCct_" 
alt="Step 8" width="360" height="270" border="10" /></a>

10. Insert the wires into the screw terminals: __Green__ _(top)_, __Brown__ _(middle)_, __White__ _(bottom)_.
11. Tighten screws. _The end result should look like the picture below:_

<a href="https://drive.google.com/uc?id=1KaIW1zzQKn3vFp1KRzFStKKjrx2n1MTn" target="_blank"><img src="https://drive.google.com/uc?id=1KaIW1zzQKn3vFp1KRzFStKKjrx2n1MTn" 
alt="Step 10" width="360" height="270" border="10" /></a>

12. Repeat the steps above with the cable from the siren box coming in from the right, and using the _male connector_.

<a href="https://drive.google.com/uc?id=1L3nnnsma9mmec42gwE4fnFGiG6R6CAoF" target="_blank"><img src="https://drive.google.com/uc?id=1L3nnnsma9mmec42gwE4fnFGiG6R6CAoF" 
alt="Step 11a" width="360" height="270" border="10" /></a>
<a href="https://drive.google.com/uc?id=1sspQ6ZHdZmt316rrHJ9sm8AHWMRysx-k" target="_blank"><img src="https://drive.google.com/uc?id=1sspQ6ZHdZmt316rrHJ9sm8AHWMRysx-k" 
alt="Step 11b" width="360" height="270" border="10" /></a>

13. It is helpful to do a test fit before screwing on the outer pieces to ensure that all of the wires are continuous _(green to green, brown to brown, white to white)_.

<a href="https://drive.google.com/uc?id=1VcGGt6fmMC3KMDC7gsqPq1645KLyb8Fh" target="_blank"><img src="https://drive.google.com/uc?id=1VcGGt6fmMC3KMDC7gsqPq1645KLyb8Fh" 
alt="Continuous" width="360" height="270" border="10" /></a>

14. Screw on the outer shell and then tighten the back nut on each connector and you're good to go!

<a href="https://drive.google.com/uc?id=1FMUgUeWEwEZRza5iL18O6s8nJc1ov4D5" target="_blank"><img src="https://drive.google.com/uc?id=1FMUgUeWEwEZRza5iL18O6s8nJc1ov4D5" 
alt="Done" width="360" height="270" border="10" /></a># Connector Setup Instructions.

## Intro:

The purpose of this document is to allow you to easily switch between our tack sensor and the Homerus tack sensor using a standard waterproof connector.

## What is included:
* Tack sensor
* Male connector
* Female connector (_X2_)

## What you will need:
* Homerus tack sensor system
* Wire cutters/strippers
* 2mm flathead screwdriver
* Soldering iron _(optional but preferred)_

### Getting Started:

1. Cut the wire off of the new tack sensor, leaving around 10-15 cm still attached.
2. Strip off about 3 cm of the outer shielding leaving the 3 inner wires exposed. _(The instructions from here on are to be used on the new and old tack sensors)_
3. Strip off about 1 cm of the shielding from the ends of each wire.
4. Tin each wire with solder for ease of installation and a more solid connection _(optional)_. _The end result should look like the picture below:_

<a href="https://drive.google.com/uc?id=1Z3OUldAsjI05VXoYu_Hq9xgZHW5KPdX4" target="_blank"><img src="https://drive.google.com/uc?id=1Z3OUldAsjI05VXoYu_Hq9xgZHW5KPdX4" 
alt="Step 1" width="360" height="270" border="10" /></a>

### Wiring the Connectors:
5. The _female connectors_ go on each __tack sensor__ while the _male connector_ go from the __siren box__.
6. Place parts of the connector over the wire in the order shown below _(from left to right)_:
    1. Black screw piece
    2. White plastic piece
    3. Black rubber seal
    4. Black plastic collar
7. Arrange wires from top to bottom: __Green__, __Brown__, __White__.
8. Position the connector such that the center screw terminal is furthest from the end of the wire.
9. Back the set screws off almost all the way _(don't take them all the way out, they are not fun to lose)_.

<a href="https://drive.google.com/uc?id=11hnMpyINCIi0deB9e45J2_QI6u2Ftel6" target="_blank"><img src="https://drive.google.com/uc?id=11hnMpyINCIi0deB9e45J2_QI6u2Ftel6" 
alt="Step 7" width="360" height="270" border="10" /></a>
<a href="https://drive.google.com/uc?id=1JPLGSSEcDkP6DoaUjF0fQliXhIcbCct_" target="_blank"><img src="https://drive.google.com/uc?id=1JPLGSSEcDkP6DoaUjF0fQliXhIcbCct_" 
alt="Step 8" width="360" height="270" border="10" /></a>

10. Insert the wires into the screw terminals: __Green__ _(top)_, __Brown__ _(middle)_, __White__ _(bottom)_.
11. Tighten screws. _The end result should look like the picture below:_

<a href="https://drive.google.com/uc?id=1KaIW1zzQKn3vFp1KRzFStKKjrx2n1MTn" target="_blank"><img src="https://drive.google.com/uc?id=1KaIW1zzQKn3vFp1KRzFStKKjrx2n1MTn" 
alt="Step 10" width="360" height="270" border="10" /></a>

12. Repeat the steps above with the cable from the siren box coming in from the right, and using the _male connector_.

<a href="https://drive.google.com/uc?id=1L3nnnsma9mmec42gwE4fnFGiG6R6CAoF" target="_blank"><img src="https://drive.google.com/uc?id=1L3nnnsma9mmec42gwE4fnFGiG6R6CAoF" 
alt="Step 11a" width="360" height="270" border="10" /></a>
<a href="https://drive.google.com/uc?id=1sspQ6ZHdZmt316rrHJ9sm8AHWMRysx-k" target="_blank"><img src="https://drive.google.com/uc?id=1sspQ6ZHdZmt316rrHJ9sm8AHWMRysx-k" 
alt="Step 11b" width="360" height="270" border="10" /></a>

13. It is helpful to do a test fit before screwing on the outer pieces to ensure that all of the wires are continuous _(green to green, brown to brown, white to white)_.

<a href="https://drive.google.com/uc?id=1VcGGt6fmMC3KMDC7gsqPq1645KLyb8Fh" target="_blank"><img src="https://drive.google.com/uc?id=1VcGGt6fmMC3KMDC7gsqPq1645KLyb8Fh" 
alt="Continuous" width="360" height="270" border="10" /></a>
