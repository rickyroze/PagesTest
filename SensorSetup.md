---
layout: page
title: Tack Sensor Setup
permalink: /sensorsetup/
---
Replacement Tack Indicator Installation, Adjustment, and Operation

\[Planned Final Version\]

Olin College Blind Sailing Summer 2018 Research Team

This replacement tack indicator sensor has been
designed as a drop-in replacement for the original tack indicator
sensors in the Homerus Autonomous Sailing System. The original sensors
are notoriously finicky and inaccurate and, more importantly, contain a
glass vial of mercury which is a major environmental and health hazard.
This document will describe how to safely and easily replace the
original sensor with our non-hazardous solid state one.

If at any time you have questions or feedback about this system, please
email

[*blindsailing@olin.edu*](mailto:blindsailing@olin.edu)

We really appreciate any feedback on your experience with setting up and
using this system, especially in this beta testing phase.

For the purposes of this beta test, we’ve also provided a set of inline
waterproof connectors to allow you to easily switch between our tack
sensor and the Homerus tack sensor. If you would like to install them,
follow the instructions in the provided “Connector Setup Instructions”
document after finishing this document.

\*\*\*Do not open the original switch\*\*\*

1.  Open the large white box with the sirens in it and disconnect the
    > battery.

2.  Cut the white wire about 10cm from the original sensor.

3.  Carefully place the entire original sensor in a plastic bag or
    > tightly-sealed container and clearly label it “MERCURY – DO NOT
    > OPEN.”

4.  Call 1-800-CLEANUP or visit
    > [*search.earth911.com*](http://search.earth911.com/) to find your
    > local hazmat disposal facility, and drop off the sensor as soon as
    > you can.

> Note: If you would like to keep the option of reverting back to the
> original switch, you may choose not to dispose of it. You can
> reinstall the original switch by opening the small white case,
> threading the wire back in, and splicing the green, brown, and white
> wires back onto the stubs left in the box (remember to disconnect the
> battery in the siren box first). If you choose to do this, please
> store the switch in a safe location and dispose of it properly when
> you are satisfied that you will not need it.
>
> For more information on dealing with mercury, visit
> [*https://www.epa.gov/mercury/storing-transporting-and-disposing-mercury-your-home*](https://www.epa.gov/mercury/storing-transporting-and-disposing-mercury-your-home)

Connect new sensor:
-------------------

1.  Thread the cable you just cut through the grey fitting in the back
    > of the waterproof case. (Leave battery in white box disconnected
    > for now)

<a href="https://drive.google.com/uc?id=1p_csmbEMzTuyT3V9WIDE_05ZfEy_YlwS" target="_blank"><img src="https://drive.google.com/uc?id=1p_csmbEMzTuyT3V9WIDE_05ZfEy_YlwS" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>


1.  Strip about 35 mm of the white insulation from the cable, being
    > careful to leave the inner insulation intact.

2.  Strip about 5-6 mm (a little less than ¼ inch) of insulation from
    > each of the three internal wires and, if possible, tin the ends of
    > the wires with a soldering iron. The image below is to scale and
    > may be used as a strip gauge.

<a href="https://drive.google.com/uc?id=14lQhh0HiCnJ4glIpzu9feD_-9wpu_DuL" target="_blank"><img src="https://drive.google.com/uc?id=14lQhh0HiCnJ4glIpzu9feD_-9wpu_DuL" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

1.  Push each of the three wires into its respective terminal on the top
    > left of the circuit board and tighten the screws with a small
    > screwdriver.

    a.  The terminals are labeled with the colors of the wires – from
        > left to right they are green (starboard siren), brown (12V
        > common connection), and white (port siren).

    <a href="https://drive.google.com/uc?id=1M_iuVG3dQJTbZcf1jxOM6OLZXYgT2HiR" target="_blank"><img src="https://drive.google.com/uc?id=1M_iuVG3dQJTbZcf1jxOM6OLZXYgT2HiR" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

    b.  If you did not tin the wires, make sure that there are no stray strands poking out of the terminal block.

    c.  Tug gently on the wires to make sure they are secure.

2.  Adjust the cable so there is a small amount of slack in the case,
    > and tighten the grey fitting on the back by hand so that it grips
    > the cable and seals out water.

3.  Ensure that switch is in the OFF (down) position.

4.  Reconnect the battery in the white box.

> You can now turn your sensor on with the switch on the side of the
> clear case and test it by tilting it side to side. You will notice the
> following indicator light functions:

-   The pair of lights at the top of the board come on in conjunction
    > with when the sirens are on. The PORT light (red; left at the top
    > of the board) comes on with the port siren when the boat is on a
    > port tack, and the STBD light (green; right at the top of the
    > board) comes on with the starboard siren on a starboard tack.

-   The BAL light (yellow; on the far right side of the board) comes on
    > when the switch is perfectly vertical (boom is exactly amidships
    > and boat is not heeling).

-   The BATT light (blue; on the far left side of the board) indicates
    > battery condition. If it is not on, the 9V battery should be
    > replaced, but the sensor may function for some time after the
    > light goes out. Note that this light only indicates the status of
    > the 9V battery, you will still need to monitor and charge the 12V
    > battery independently.

> The switch on the side of the clear case controls both the new 9V
> system and the old 12V system that powers the sirens, so there is no
> need to disconnect the battery in the white box except for service and
> charging.

Install new sensor on boat
--------------------------

You have a number of options for attaching your new sensor to your boat.
At this point your new sensor should behave exactly like the mercury
sensor did. If you have a way of mounting it to the boom that you are
happy with, you can connect it in exactly the same way. Just make sure
that the cable points toward the bow of the boat and the 9V battery is
in the bottom part of the box. If you choose to remove the carabiner on
the top of the case, note that a silicone sealant was used to seal
around the bolts and should be replaced to maintain waterproofness.

For our recommended mounting options, please see the photos on the next
pages. We have found that this mounting geometry provides the most
accurate reading. Mounting the sensor parallel to the boom vang does not
work well because the absolute angle between the sensor and vertical
does not change as the boom moves.

With any configuration, make sure to attach the cable to the boom and
the mast to keep it from getting damaged or tangled in the rigging.
Cable ties are always a good option.

\[BETA\]

Your boat will inevitably need something slightly different from what we
have experienced, so please send us any notes or images about your setup
so that we can provide more detailed instructions in the future. If you
aren’t able to figure out a way to mount it, please email us at
[*blindsailing@olin.edu*](mailto:blindsailing@olin.edu) with some photos
of your boat and we will do our best to troubleshoot with you.

### \

### Mounting on a Sonar

This configuration or similar should work on most models of boat where
the triangle formed by the boom vang, mast, and boom is fairly large.
The bracket should be attached to the mast with the provided track stop
or your own. The bracket may be easily removed while leaving the track
stop permanently in the luff groove by simply loosening the bottom bolt,
sliding the bracket off, and securing the bolt again. Do not fully
remove the bolt from the track stop as the track stop will fall to the
bottom of the luff groove, where it will be difficult to recover. The
top bolt in the bracket is not meant to be removed, it is there to align
the bracket with the luff groove. Note that the effective length of the
shock cord is extended by going through the bracket and all the way to
the mast. Tie the shock cord with a taut line hitch so it can be
adjusted.

<a href="https://drive.google.com/uc?id=1TGvSc0fXox1GkBelW6H_OLSD2GtyLxXS" target="_blank"><img src="https://drive.google.com/uc?id=1TGvSc0fXox1GkBelW6H_OLSD2GtyLxXS" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>


### Mounting on a Rhodes 19

This configuration or similar should work on most models of boat where
the foredeck comes aft past the point where the boom vang is attached to
the boom. Note that the effective length of the shock cord is extended
by going through the loop and all the way to the mast. Tie the shock
cord with a taut line hitch so it can be adjusted. This method is more
accurate and should be used if possible.

<a href="https://drive.google.com/uc?id=1LJK4ZzjazSOMYzuss8Vc7vvzJa8-cDGu" target="_blank"><img src="https://drive.google.com/uc?id=1LJK4ZzjazSOMYzuss8Vc7vvzJa8-cDGu" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>


Adjust sensor for your boat (optional)
--------------------------------------

Your new sensor comes calibrated to match the trigger points of the
original mercury sensor, but if you want to customize it for more
accurate operation, you may do so while maintaining the option to revert
easily at any time to the default settings.

When adjusting this sensor, we recommend that you disconnect the 12V
battery in the large white box, as this will stop the siren from
sounding and you will still be able to observe the operation and trigger
points by looking at the LED lights on the circuit board. You can also
silence the sirens by putting the DIP switch in the large white box at
position 8 (the top switch) in the ON (right) position. This allows you
to see that the signal is reaching the white box because the lights in
the white box will flash when one of the sirens would be activated.

To adjust the sensor you will need to mount it on a boat with your
preferred method, connect the 9V battery to the battery clip, and turn
the switch on the outside of the box to its ON (up) position.

### Default Mode

To return to default mode (mimics mercury sensor) at any time, place DIP
switch 1 (the top one) in the OFF position (to the left). This disables
the BALANCE, THRESHOLD, and BUFFER adjustments and sets the values they
control to be identical to the original mercury sensor.

### Custom Mode adjustment

1.  Set DIP switch 1 to ON (switch 2 is not configured to do anything).

2.  Use a small screwdriver to carefully adjust the BALANCE
    > potentiometer so that the BAL light comes on or flickers when the
    > boom is amidships and the boat is not heeling.

3.  Turn the BUFFER and THRESHOLD potentiometers all the way
    > counterclockwise (make sure you don’t force them when they reach
    > the end of their range).

4.  Move the boom by hand to the point at which you would like the
    > indicator to trigger. The PORT or STBD light should be on.

5.  Slowly turn the THRESHOLD potentiometer clockwise until the PORT or
    > STBD light goes out. You may adjust it back and forth until you
    > find the precise place you would like the indicator to trigger.

6.  Now turn the BUFFER potentiometer clockwise about ¼ turn. This
    > potentiometer should be adjusted by feel. It controls how much
    > “grey area” there is around the trigger point. If it is turned
    > more clockwise, the indicator will be more sluggish to respond to
    > small changes in angle near the trigger point. More
    > counterclockwise and the indicator may tend to turn on and off
    > with very small changes in angle when the boom is near the trigger
    > point.
